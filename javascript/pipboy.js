var PIPBOY = PIPBOY || {};

PIPBOY.methods = {
	currentDirectory: 0,
	currentDirectoryString: "",
	response: "",
	
	//gets the inserted value
	getInput: function(e){	
		e = e || window.event;
		var inputTextValue = e.target.value;
		var splitValue = inputTextValue.split(" ");

		if(e.keyCode === 9){
			e.preventDefault();
			PIPBOY.methods.autoComplete(splitValue);
		}
		 
		 //check if enter was hit
		if(e.keyCode === 13){
				switch(splitValue[0]){
					case "ls":
						PIPBOY.methods.listContents(splitValue);
						break;
					case "cd":
						PIPBOY.methods.changeDirectory(splitValue);
						break;
					case "cat":
						PIPBOY.methods.getContents(splitValue);
						break;
					default:
						PIPBOY.methods.notMethod(splitValue);
						break;
				}
		}

		return false;
	},
	
	//lists the contents of given directory
	listContents: function(value){
		var node = 0;
		if(value[1] != null){
				node = PIPBOY.methods.dirStringToId(value[1]);
		}else{
			node = PIPBOY.methods.currentDirectory;
		}
		
		if(node != "false"){
			var response = '';
			var xhr = new XMLHttpRequest();

			xhr.onreadystatechange = function () {
				if (this.readyState == 4 && this.status == 200) {
					response = this.responseText;
					var result = JSON.parse(response);
					var output = "<span class='terminal-row-result'>";
					for (var prop in result.items) {
						if(result.items[prop].type != "folder"){
							output += result.items[prop].name + " ";
						}else{
							output += "<strong>" + result.items[prop].name + "</strong> ";
						}
					}
					output += "</span>";
					var cmd = "<span class='terminal-row'><span class='dir-loc'>~" + PIPBOY.methods.currentDirectoryString + "</span><span class='cursor-beg'>$</span>";
					for(var i = 0; i < value.length; i++)
						cmd += value[i] + " ";
					cmd += "</span>";
					$("#terminal-results").append(cmd);
					$("#terminal-results").append(output);
					document.getElementById("terminal-input").value = "";
				}
			}
			xhr.open('GET', 'http://localhost:8080/api/ls/' + node, true);
			xhr.send(null);	
		}else{
			var cmd = "<span class='terminal-row'><span class='dir-loc'>~" + PIPBOY.methods.currentDirectoryString + "</span><span class='cursor-beg'>$</span>";
				for(var i = 0; i < value.length; i++)
					cmd += value[i] + " ";
			cmd += "</span>";
			var output = "Could not find directory";
			$("#terminal-results").append(cmd);
			$("#terminal-results").append(output);
			$("#active-terminal .dir-loc").html("~" + PIPBOY.methods.currentDirectoryString);
			document.getElementById("terminal-input").value = "";
		}
	},
	
	//changes the current working directory
	changeDirectory: function(dir) {
		if(dir[1] != ".."){
			var dirId = PIPBOY.methods.dirStringToId(dir[1]);
			if(dirId >= 0)
				PIPBOY.methods.setCurrentDirectory(dirId, dir[1]);
		}else{
			PIPBOY.methods.setCurrentDirectory(0, dir[1]);
		}
		
		var output = "";
		var value = dir;
		var cmd = "<span class='terminal-row'><span class='dir-loc'>~" + PIPBOY.methods.currentDirectoryString + "</span><span class='cursor-beg'>$</span>";
		for(var i = 0; i < value.length; i++)
			cmd += value[i] + " ";
		cmd += "</span>";
		$("#terminal-results").append(cmd);
		$("#terminal-results").append(output);
		$("#active-terminal .dir-loc").html("~" + PIPBOY.methods.currentDirectoryString);
		document.getElementById("terminal-input").value = "";
	},
	
	//gets the contents of the file requested
	getContents: function(value) {
		var fileId = PIPBOY.methods.fileInDir(value[1]);
			
		if(fileId != "false"){
			var response = '';
			var xhr = new XMLHttpRequest();

			xhr.onreadystatechange = function () {
				if (this.readyState == 4 && this.status == 200) {
					response = this.responseText;
					var result = JSON.parse(response);
					var output = "<span class='terminal-row-result'>";
					for (var prop in result) {
							output += result.content.replace(/\n/g, "<br>") + " ";
					}
					output += "</span>";
					var cmd = "<span class='terminal-row'><span class='dir-loc'>~" + PIPBOY.methods.currentDirectoryString + "</span><span class='cursor-beg'>$</span>";
					for(var i = 0; i < value.length; i++)
						cmd += value[i] + " ";
					cmd += "</span>";
					$("#terminal-results").append(cmd);
					$("#terminal-results").append(output);
					document.getElementById("terminal-input").value = "";
				}
			}
			
			xhr.open('GET', 'http://localhost:8080/api/cat/' + fileId, true);
			xhr.send(null);
		}else{
			var cmd = "<span class='terminal-row'><span class='dir-loc'>~" + PIPBOY.methods.currentDirectoryString + "</span><span class='cursor-beg'>$</span>";
				for(var i = 0; i < value.length; i++)
					cmd += value[i] + " ";
			cmd += "</span>";
			var output = "File does not exist";
			$("#terminal-results").append(cmd);
			$("#terminal-results").append(output);
			$("#active-terminal .dir-loc").html("~" + PIPBOY.methods.currentDirectoryString);
			document.getElementById("terminal-input").value = "";
			
		}
	},
	
	autoComplete: function(value) {
		
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				response = this.responseText;
				var result = JSON.parse(response);
				PIPBOY.methods.response = result;
			}
		}
		
		xhr.open('GET', 'http://localhost:8080/api/autocomplete/' + PIPBOY.methods.currentDirectory + '/' + value[1], true);
		xhr.send(null);	
		
		var i = 0;	
		var result = PIPBOY.methods.response;
		for (var prop in result.items) {
			if (result.items[prop].name.indexOf(value[1]) >= 0)
				$("#active-terminal #terminal-input").val(value[0] + " " + result.items[prop].name);
			i++;
		}
		return false;
		
	},
	
	notMethod: function(value) {
		var cmd = "<span class='terminal-row'><span class='dir-loc'>~" + PIPBOY.methods.currentDirectoryString + "</span><span class='cursor-beg'>$</span>";
				for(var i = 0; i < value.length; i++)
					cmd += value[i] + " ";
			cmd += "</span>";
			var output = "No command for " + value[0];
			$("#terminal-results").append(cmd);
			$("#terminal-results").append(output);
			$("#active-terminal .dir-loc").html("~" + PIPBOY.methods.currentDirectoryString);
			document.getElementById("terminal-input").value = "";
	},

	//sets the current directory
	setCurrentDirectory: function(value, string) {
		PIPBOY.methods.currentDirectory = value;
		
		if(value > 0)
			PIPBOY.methods.currentDirectoryString = "/" + string;
		else
			PIPBOY.methods.currentDirectoryString = "";

	},
	
	//get the directory id from name
	dirStringToId: function(value) {
		
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				response = this.responseText;
				var result = JSON.parse(response);
				PIPBOY.methods.response = result;
			}
		}
		xhr.open('GET', 'http://localhost:8080/api/ls/' + PIPBOY.methods.currentDirectory, true);
		xhr.send(null);	
		
		var i = 0;	
		var result = PIPBOY.methods.response;
		for (var prop in result.items) {
			if(result.items[prop].type == "folder" && result.items[prop].name == value){
				return Object.keys(result.items)[i];
			}
			i++;
		}
		return "false";
		
	},
	
	//check if file is in correct directory
	fileInDir: function(value) {
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				response = this.responseText;
				var result = JSON.parse(response);
				PIPBOY.methods.response = result;
				

			}
		}
		
		xhr.open('GET', 'http://localhost:8080/api/ls/' + PIPBOY.methods.currentDirectory, true);
		xhr.send(null);	
		
		var i = 0;	
		var result = PIPBOY.methods.response;
		//console.log(Object.keys(result.items)[1]);
		for (var prop in result.items) {
			if(result.items[prop].type == "file" && result.items[prop].name == value){
				return Object.keys(result.items)[i];
			}
			i++;
		}
		return "false";
	}
}